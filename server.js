// set the user registeration varibale
var UserRegistry = require('./user-registry.js');
// set the user session with informatiom
var UserSession = require('./user-session.js');

// store global variables
var userRegistry = new UserRegistry();
var rooms = {};

var express = require('express');

/*
 * Server startup
*/
var app = express();

var port = 443;

var path = require('path');
var fs = require('fs');

var options = {
  key:  fs.readFileSync('keys/key.key'),
  cert: fs.readFileSync('keys/crt.crt')
};


var https = require('https');
var httpsServer;
var httpsServer = https.createServer(options, app).listen(port, function () {
    console.log('started---');
});

app.use(express.static(__dirname + '/'));
app.use(express.static(path.join(__dirname, 'static')));

var io = require('socket.io')(httpsServer);

/**
 * Message handlers
 */
io.on('connection', function (socket) {

    socket.emit('id', socket.id);

    socket.on('error', function (data) {
        leaveRoom(socket.id);
    });

    socket.on('disconnect', function (data) {
        leaveRoom(socket.id);
    });

    socket.on('message', function (message) {
	console.log(JSON.stringify(message));
	if ( message.data.id == 'register' ) {
	    getRoom(message.roomName, function(err, room){
		if ( err )
		    return;
		var userSession = new UserSession(socket, message.roomName);
		userRegistry.register(userSession);
		room.participants[socket.id] = userSession;
		userSession.sendMessage({
        	    id: 'existingParticipants',
        	    count: Object.keys(room.participants).length
		});
	    });
	} else {
	    getRoom(message.roomName, function(err, room){
		if ( err )
		    return;
		var usersInRoom = room.participants;
		for ( var i in usersInRoom ){
		    if ( usersInRoom[i].id != socket.id )
			usersInRoom[i].sendMessage(message.data);
		}
	    });
	}
    });
});

function getRoom(roomName, callback) {
    var room = rooms[roomName];
    if (room == null) {
	room = {
        	name: roomName,
                participants: {},
        };
	rooms[roomName] = room;
	callback(null, room);
    } else {
        callback(null, room);
    }
}

function leaveRoom(sessionId) {

    var userSession = userRegistry.getById(sessionId);
    if (!userSession)
        return;

    var roomName = userSession.roomName;
    var room = rooms[roomName];
    if (!room) {
        return;
    }

    var usersInRoom = room.participants;

    var data = {
        id: 'participantLeft'
    };

    delete usersInRoom[userSession.id];
    
    userRegistry.unregister(sessionId);
    
    if ( Object.keys(room.participants).length == 0 )
        delete rooms[roomName];
}
