// Generate random room name if neededv

if (!location.hash) {
  location.hash = Math.floor(Math.random() * 0xFFFFFF).toString(16);
}

var socket = io.connect();

const roomHash = location.hash.substring(1);
var sessionId = null;


// TODO: Replace with your own channel ID
const roomName = 'room-' + roomHash;
const configuration = {
  iceServers: [/*{
    urls: ['stun:stun.l.google.com:19302','stun:172.217.31.30:19302']
  },*/
//    {urls:["turn:54.39.151.79?transport=udp"],username:"turn916004",credential:"turn943458"}
//    {urls:["turn:numb.viagenie.ca"],username:"webrtc@live.com",credential:"muazkh"}
//    {urls:["turn:167.114.185.160"],username:"turn783061",credential:"turn181068"}
    {urls:[
          "stun:stun.l.google.com:19302",
          "stun:stun1.l.google.com:19302",
          "stun:stun2.l.google.com:19302",
          "stun:stun3.l.google.com:19302",
          "stun:stun4.l.google.com:19302",
          "stun:stun.services.mozilla.com",
          "turn:turn.anyfirewall.com:443?transport=tcp"
        ], username:"webrtc", credential:"webrtc"}
    ]
};
let room;
let pc;

socket.on("id", function (id) {
    var bReset = false;

    if ( sessionId != null ) {
	bReset = true;
    }
    sessionId = id;
    if ( bReset ) {
	register();
    }
});
register();
/**
 * Register to server
 */
function register() {
    var data = {
        id: "register"
    };
    sendMessage(data);
}

socket.on("message", function (message) {
    console.log(JSON.stringify(message));
    if ( message.id == "existingParticipants" ){
        if ( message.count === 2 )
	    startWebRTC(true);
	else if ( message.count === 1 )
	    startWebRTC(false);
    } else {
	if (message.sdp) {
    	    // This is called after receiving an offer or answer from another peer
    	    pc.setRemoteDescription(new RTCSessionDescription(message.sdp), () => {
    		// When receiving an offer lets answer it
    		if (pc.remoteDescription.type === 'offer') {
        	    pc.createAnswer().then(localDescCreated).catch(onError);
    		}
    	    }, onError);
	} else if (message.candidate) {
    	    // Add the new ICE candidate to our connections remote description
    	    pc.addIceCandidate(message.candidate).catch(onError);
//    		new RTCIceCandidate(message.candidate), onSuccess, onError
//    	    );
	}
    }
});

function onSuccess() {};
function onError(error) {
  console.error(error);
};

// Send signaling data
function sendMessage(data) {
    console.log(JSON.stringify({ roomName: roomName, data:data }));
    socket.emit("message", { roomName: roomName, data:data });
}

function startWebRTC(isOfferer) {
  pc = new RTCPeerConnection(configuration);

  // 'onicecandidate' notifies us whenever an ICE agent needs to deliver a
  // message to the other peer through the signaling server
  pc.onicecandidate = event => {
    if (event.candidate) {
      sendMessage({'candidate': event.candidate});
    }
  };

  // If user is offerer let the 'negotiationneeded' event create the offer
  if (isOfferer) {
    pc.onnegotiationneeded = () => {
      pc.createOffer().then(localDescCreated).catch(onError);
    }
  }

  
  // When a remote stream arrives display it in the #remoteVideo element
  pc.ontrack = event => {
    console.log('track');
    const stream = event.streams[0];
    if (!remoteVideo.srcObject || remoteVideo.srcObject.id !== stream.id) {
      remoteVideo.srcObject = stream;
    }
  };
  pc.oniceconnectionstatechange = event => {
    console.log('con:'+pc.iceConnectionState);
  };
  pc.onicegatheringstatechange = event => {
    console.log('gather:'+pc.iceGatheringState);
  };
  if (isOfferer) {
  navigator.mediaDevices.getUserMedia({
    audio: true,
    video: true,
  }).then(stream => {
    // Display your local video in #localVideo element
    localVideo.srcObject = stream;
    localVideo.muted = true;
    // Add your stream to be sent to the conneting peer
    stream.getTracks().forEach(track => pc.addTrack(track, stream));
  }, onError);
}
}

function localDescCreated(desc) {
  pc.setLocalDescription(
    desc,
    () => sendMessage({'sdp': pc.localDescription}),
    onError
  );
}
