function UserSession( socket, roomName) {
    this.id = socket.id;
    this.socket = socket;
    this.roomName = roomName;
}

UserSession.prototype.sendMessage = function (data) {
    this.socket.emit('message', data);
};

module.exports = UserSession;